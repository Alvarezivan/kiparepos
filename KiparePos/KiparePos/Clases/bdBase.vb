﻿Imports System.Data.SqlClient
Imports System.Data

Public Class bdBase
    Public Shared Function bdDataReader(ByVal cadConexion As String, ByVal comando As String) As SqlDataReader
        Dim cnX As New SqlConnection(cadConexion)
        Dim cmd As New SqlCommand(comando, cnX)
        Dim dr As SqlDataReader = Nothing
        Try
            cnX.Open()
            cmd.CommandTimeout = 0
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
            'Throw ex
            Return Nothing
        Finally
            'cmd.Dispose()
            'If cnX.State = ConnectionState.Open Then cnX.Close()
            'cnX.Dispose()
        End Try
        Return dr
    End Function

    Public Shared Function bdDataset(ByVal cadConexion As String, ByVal cadQuery As String, Optional ByVal esSP As Boolean = False) As DataSet
        Dim cnX As New SqlConnection(cadConexion)
        Dim cmd As New SqlCommand(cadQuery, cnX)
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet
        Try
            cnX.Open()
            If esSP Then cmd.CommandType = CommandType.StoredProcedure
            da.SelectCommand.CommandTimeout = 0
            da.Fill(ds)
        Catch ex As Exception
            Throw ex
            Return Nothing
        Finally
            da.Dispose()
            cmd.Dispose()
            cnX.Close()
            cnX.Dispose()
        End Try
        Return ds
    End Function

    Public Shared Function bdDataset(ByVal cadConexion As String, ByVal oCommand As SqlCommand, Optional ByVal esSP As Boolean = False) As DataSet
        Dim cnX As New SqlConnection(cadConexion)
        oCommand.Connection = cnX
        If esSP Then oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandTimeout = 60
        Dim da As New SqlDataAdapter(oCommand)
        Dim ds As New DataSet
        Try
            cnX.Open()
            da.SelectCommand.CommandTimeout = 0
            da.Fill(ds)
        Catch ex As Exception
            Throw ex
            Return Nothing
        Finally
            da.Dispose()
            oCommand.Dispose()
            cnX.Close()
            cnX.Dispose()
        End Try
        Return ds
    End Function

    Public Shared Function bdExecute(ByVal cadConexion As String, ByVal cadQuery As String, Optional ByVal esSP As Boolean = False, Optional ByVal regresaId As Boolean = False) As String
        Dim regresa As String = ""
        Dim cnX As New SqlConnection(cadConexion)
        Dim cmd As New SqlCommand(cadQuery, cnX)
        Try
            cnX.Open()
            If esSP Then cmd.CommandType = CommandType.StoredProcedure
            Dim resultado As String
            If regresaId Then
                resultado = cmd.ExecuteScalar
                regresa = resultado
            Else
                cmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            regresa = "ERR. " & ex.Message
            Throw ex
        Finally
            cmd.Dispose()
            cnX.Close()
            cnX.Dispose()
        End Try
        Return regresa
    End Function

    Public Shared Function bdExecute(ByVal cadConexion As String, ByVal oCommand As SqlCommand) As String
        Dim regresa As String = ""
        Dim cnX As New SqlConnection(cadConexion)
        oCommand.Connection = cnX
        oCommand.CommandTimeout = 0
        Try
            cnX.Open()
            oCommand.ExecuteNonQuery()
        Catch ex As Exception
            regresa = ex.Message
            Throw ex
        Finally
            cnX.Close()
            cnX.Dispose()
        End Try
        Return regresa
    End Function

    Public Shared Function bdFechaSvr(ByVal cadConexion As String) As Date
        Dim regresa As Date
        Dim dr As SqlDataReader = bdDataReader(cadConexion, "SELECT Getdate()")
        If Not IsNothing(dr) Then
            If dr.HasRows Then
                dr.Read()
                regresa = dr.GetDateTime(0).Date
            End If
            dr.Close()
        End If
        Return regresa
    End Function

    Public Shared Function bdHoraSvr(ByVal cadConexion As String) As Date
        Dim regresa As Date
        Dim dr As SqlDataReader = bdDataReader(cadConexion, "SELECT {fn current_time()}")
        If Not IsNothing(dr) Then
            If dr.HasRows Then
                dr.Read()
                regresa = dr(0)
            End If
            dr.Close()
        End If
        Return regresa
    End Function

    Public Shared Function bdFechaHoraSvr(ByVal cadConexion As String) As DateTime
        Dim regresa As DateTime
        Dim dr As SqlDataReader = bdDataReader(cadConexion, "SELECT getdate()")
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                regresa = dr(0)
            End If
            dr.Close()
        End If
        Return regresa
    End Function
End Class
