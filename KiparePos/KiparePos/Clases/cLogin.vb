﻿Imports System.Data.SqlClient
'Imports System.Data.SqlServerCe

'Imports Microsoft.Win32

Public Class cLogin
    Private empId As Integer = -9
    Private empNombre As String = String.Empty
    Private empFactNo As Integer = -9
    Private empConexion As String = String.Empty
    Private empConexionL As String = String.Empty
    'Private empConexionActiva As String = String.Empty
    Private cadenaAlmacen As String = String.Empty
    Private tienePrivacidad As Boolean = False
    '
    Private usrId As Integer = -9
    Private usrNombre As String = String.Empty
    Private usrNombreC As String = String.Empty
    Private usrTipo As Integer = -9
    Private usrTienda As Integer = -9
    Private usrCaja As Integer = -9
    Private usrCambiarClave As Boolean = False
    Private loginStatus As Boolean = False
    Private usrDemo As Boolean = True
    Private superUser As Boolean = False
    Private passExpira As DateTime
    Private des As New cTripleDES
    Private mensaje As String = String.Empty
    Private tmpClave As String = String.Empty

#Region "Propiedades de Empresa"
    Public ReadOnly Property pEmpId() As Integer
        Get
            Return empId
        End Get
    End Property

    Public ReadOnly Property pEmpNombre() As String
        Get
            Return empNombre
        End Get
    End Property

    Public ReadOnly Property pEmpFactNo() As Integer
        Get
            Return empFactNo
        End Get
    End Property

    Public ReadOnly Property pEmpConexion As String
        Get
            Return empConexion
        End Get
    End Property

    Public ReadOnly Property pEmpConexionL As String
        Get
            Return empConexionL
        End Get
    End Property

    Public ReadOnly Property pCadenaAlmacen As String
        Get
            Return cadenaAlmacen
        End Get
    End Property

    'Public ReadOnly Property pEmpConexionActiva As String
    '    Get
    '        Return empConexionActiva
    '    End Get
    'End Property

    Public ReadOnly Property pTienePrivacidad() As Boolean
        Get
            Return tienePrivacidad
        End Get
    End Property
#End Region

#Region "Propiedades de empleado"
    Public ReadOnly Property pUserId() As Integer
        Get
            Return usrId
        End Get
    End Property

    Public ReadOnly Property pUsrNombre() As String
        Get
            Return usrNombre
        End Get
    End Property

    Public ReadOnly Property pUsrNombreC() As String
        Get
            Return usrNombreC
        End Get
    End Property

    Public ReadOnly Property pUsrTipo() As Integer
        Get
            Return usrTipo
        End Get
    End Property

    Public ReadOnly Property pUsrTienda() As Integer
        Get
            Return usrTienda
        End Get
    End Property

    Public ReadOnly Property pUsrCaja() As Integer
        Get
            Return usrCaja
        End Get
    End Property

    Public ReadOnly Property pUsrCambiarClave As Boolean
        Get
            Return usrCambiarClave
        End Get
    End Property

    Public ReadOnly Property pLoginStatus() As Boolean
        Get
            Return loginStatus
        End Get
    End Property

    Public ReadOnly Property pUsrDemo() As Boolean
        Get
            Return usrDemo
        End Get
    End Property

    Public ReadOnly Property pUsrSuper() As Boolean
        Get
            Return superUser
        End Get
    End Property

    Public ReadOnly Property pPassExpira As String
        Get
            Return passExpira
        End Get
    End Property
#End Region

    Public ReadOnly Property pMensaje As String
        Get
            Return mensaje
        End Get
    End Property

    Public Sub New()
        MyBase.New()
    End Sub

    Public Function doLoginUser(usuario As String, clave As String, Optional tEmp As Integer = -1, Optional loginConectado As Boolean = True) As Integer
        If String.IsNullOrEmpty(usuario) Or String.IsNullOrEmpty(clave) Then
            Return -1
        End If
        ' Bitacora.SaveTextToFile("Inicia doLoginUser", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim regresa As Integer = -1
        Dim des As New cTripleDES
        Dim decriptedPass As String = des.Decrypt("MK9vq/6UwNi5C1CWL6VdVw==")
        Dim encryptedPass As String = des.Encrypt("")
        Dim sQuery As String = String.Empty
        If usuario.Trim.ToLower = "soporte@elsaz.com" Or usuario.Trim.ToLower = "soporte@secuencia.com" Then
            sQuery = "SELECT admin FROM configuracion where admin = '" & clave & "'"
            Dim dr As SqlDataReader = bdBase.bdDataReader(des.Decrypt(My.Settings.conLogins), sQuery)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    usrNombre = usuario
                    usrNombreC = "SAZ"
                    superUser = True
                    regresa = tEmp
                    empId = tEmp
                Else
                    loginStatus = False
                    mensaje = "Usuario/Clave incorrectos"
                End If
                dr.Close()
            End If
        Else
            ' Usuario normal
            sQuery = "SELECT id, Empresa, Activo, Suspendido, Accesos, Expira FROM Rlogin WHERE Usuario = '" & usuario & "' AND Clave = '" & des.Encrypt(clave) &
                         "' AND Empresa = " & My.Settings.idEmpresa
            Dim dr As SqlDataReader
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    usrNombre = usuario
                    regresa = dr("Empresa")
                    empId = regresa
                    superUser = False
                Else
                    loginStatus = False
                    mensaje = "Usuario/Clave incorrectos"
                End If
                dr.Close()
            End If
        End If
        Return regresa
    End Function

    Public Sub doLoginEmp(empresa As Integer)
        Dim sQuery As String
        Dim des As New cTripleDES
        sQuery = "SELECT Empresa, Server, UsuarioSvr, PassSvr, BasedeDatos, svrLocal, usrLocal, pwdLocal, bddLocal, Facturador FROM logins WHERE idEmpresa = " &
                     empresa & " AND Status = 1"
        Dim dr2 As SqlDataReader = bdBase.bdDataReader(des.Decrypt(My.Settings.conLogins), sQuery)
        If Not dr2 Is Nothing Then
            If dr2.HasRows Then
                dr2.Read()
                ' Datos de empresa
                empId = empresa
                If Not IsDBNull(dr2("Empresa")) Then empNombre = dr2("Empresa").ToString.Trim
                If Not IsDBNull(dr2("Server")) And Not IsDBNull(dr2("UsuarioSvr")) And Not IsDBNull(dr2("PassSvr")) And Not IsDBNull(dr2("BasedeDatos")) Then
                    empConexion = "Data Source=" & dr2("Server").ToString.Trim & ";Initial Catalog=" & dr2("BasedeDatos").ToString.Trim &
                                      ";Persist Security Info=True;User ID=" & dr2("UsuarioSvr").ToString.Trim &
                                      ";password=" & dr2("PassSvr").ToString.Trim & ";Application Name=""AIO by SAZ"""
                    My.Settings.conexion = empConexion
                    My.Settings.Save()
                End If
                ' Obtener cadena almacen
                ' complemento empleado
                If usrNombre <> "soporte@elsaz.com" And usrNombre <> "soporte@secuencia.com" Then
                    sQuery = "SELECT id, Nombre, Tipo, Tienda, eMail FROM empleado WHERE UPPER([user]) = '" & usrNombre.ToUpper & "'"
                    Dim dr As SqlDataReader = bdBase.bdDataReader(empConexion, sQuery)
                    If Not IsNothing(dr) Then
                        If dr.HasRows Then
                            dr.Read()
                            usrId = dr("id")
                            ' usrNombre = 
                            usrNombreC = dr("Nombre").ToString.Trim
                            usrTipo = dr("Tipo")
                            usrTienda = dr("Tienda")
                            loginStatus = True
                        Else
                            empId = -9
                            loginStatus = False
                        End If
                        dr.Close()
                    End If
                Else
                    usrId = -1
                    usrNombreC = "SAZ"
                    usrTipo = "1"
                    usrTienda = My.Settings.TiendaActual
                End If
                '
                If empId <> -9 Then
                    loginStatus = True
                    mensaje = ""
                Else
                    mensaje = "Usuario o clave incorrectos."
                End If
            Else
                loginStatus = False
                mensaje = "Empresa inactiva. Contacte a ventas@elsaz.com"
            End If
            dr2.Close()
        End If
    End Sub
End Class
