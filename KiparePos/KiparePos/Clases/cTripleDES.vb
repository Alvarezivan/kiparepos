﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Public Class cTripleDES
    ' define the triple des provider

    Private m_des As New TripleDESCryptoServiceProvider

    ' define the string handler

    Private m_utf8 As New UTF8Encoding

    ' define the local property arrays

    Private m_key() As Byte = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
    Private m_iv() As Byte = {8, 7, 6, 5, 4, 3, 2, 1}

    Public Sub New()  ' ByVal key() As Byte, ByVal iv() As Byte
        'Me.m_key = key
        'Me.m_iv = iv
    End Sub

    Public Function Encrypt(ByVal input() As Byte) As Byte()
        Return Transform(input, m_des.CreateEncryptor(m_key, m_iv))
    End Function

    Public Function Decrypt(ByVal input() As Byte) As Byte()
        Return Transform(input, m_des.CreateDecryptor(m_key, m_iv))
    End Function

    Public Function Encrypt(ByVal text As String) As String
        Dim input() As Byte = m_utf8.GetBytes(text)
        Dim output() As Byte = Transform(input, _
                        m_des.CreateEncryptor(m_key, m_iv))
        Return Convert.ToBase64String(output)
    End Function

    Public Function Decrypt(ByVal text As String) As String
        Dim input() As Byte = Convert.FromBase64String(text)
        Dim output() As Byte = Transform(input, _
                         m_des.CreateDecryptor(m_key, m_iv))
        Return m_utf8.GetString(output)
    End Function

    Private Function Transform(ByVal input() As Byte, _
        ByVal CryptoTransform As ICryptoTransform) As Byte()
        ' create the necessary streams

        Dim memStream As MemoryStream = New MemoryStream
        Dim cryptStream As CryptoStream = New  _
            CryptoStream(memStream, CryptoTransform, _
            CryptoStreamMode.Write)
        ' transform the bytes as requested

        cryptStream.Write(input, 0, input.Length)
        cryptStream.FlushFinalBlock()
        ' Read the memory stream and convert it back into byte array

        memStream.Position = 0
        Dim result(CType(memStream.Length - 1, System.Int32)) As Byte
        memStream.Read(result, 0, CType(result.Length, System.Int32))
        ' close and release the streams

        memStream.Close()
        cryptStream.Close()
        ' hand back the encrypted buffer

        Return result
    End Function

    Public Function GetSHA1(ByVal cadena As String) As String
        Dim sha1 As SHA1 = SHA1Managed.Create()
        Dim encoding As New ASCIIEncoding()
        Dim stream As Byte()
        Dim sb As New StringBuilder()
        stream = sha1.ComputeHash(encoding.GetBytes(cadena))
        For i As Integer = 0 To stream.Length - 1
            sb.AppendFormat("{0:x2}", stream(i))
        Next
        Return sb.ToString.ToUpper
    End Function

    'Public Function generarClaveSHA1(ByVal nombre As String) As String
    '    ' Crear una clave SHA1 como la generada por 
    '    ' FormsAuthentication.HashPasswordForStoringInConfigFile
    '    ' Adaptada del ejemplo de la ayuda en la descripción de SHA1 (Clase)
    '    Dim enc As New UTF8Encoding
    '    Dim data() As Byte = enc.GetBytes(nombre)
    '    Dim result() As Byte

    '    Dim sha As New SHA1CryptoServiceProvider()
    '    ' This is one implementation of the abstract class SHA1.
    '    result = sha.ComputeHash(data)
    '    '
    '    ' Convertir los valores en hexadecimal
    '    ' cuando tiene una cifra hay que rellenarlo con cero
    '    ' para que siempre ocupen dos dígitos.
    '    Dim sb As New StringBuilder
    '    For i As Integer = 0 To result.Length - 1
    '        If result(i) < 16 Then
    '            sb.Append("0")
    '        End If
    '        sb.Append(result(i).ToString("x"))
    '    Next
    '    '
    '    Return sb.ToString.ToUpper
    'End Function
End Class
